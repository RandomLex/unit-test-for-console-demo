import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * @author alexej_barzykin@epam.com
 */
public class DoroshenkoQuestion001Test {

  private static final String[] EMPTY_ARGS = {};
  //We could just use /r/n here. But for linux the new line is \n.
  //Fortunately, we have a special system property, which we can retrieve using the following.
  private static final String LINE_DELIMITER = System.getProperty("line.separator");
  private static InputStream sysInBackup;
  private static OutputStream sysOutBackup;
  private ByteArrayOutputStream outStream;


  //Will be run ones before all the tests.  The name of method is not important but annotation @BeforeClass is.
  @BeforeClass
  public static void setUp() {
    // backup System.in to restore it later
    sysInBackup = System.in;
    sysOutBackup = System.out;
  }

  //Will be run ones after all the tests. The name of method is not important but annotation @AfterClass is.
  @AfterClass
  public static void tearDown() {
    System.setIn(sysInBackup); //Restore the reading from console.
    // In fact it is not so necessary in tests. It's rather to let you know how to revert the initial state.
    System.setOut((PrintStream) sysOutBackup);
  }

  //Will be run before each test. The name of method is not important but annotation @Before is.
  @Before
  public void beforeEachTest() {
    outStream = new ByteArrayOutputStream();
    System.setOut(new PrintStream(outStream));
  }


  @Test
  public void mainExpectsGreen() {
    //given -- is already set up in the setUp and before methods

    //when
    ByteArrayInputStream in = new ByteArrayInputStream("1.5".getBytes());
    System.setIn(in);
    DoroshenkoQuestion001.main(EMPTY_ARGS);

    //then
    Assert.assertEquals("зелёный" + LINE_DELIMITER, outStream.toString());
  }

  @Test
  public void mainExpectsYellow() {
    //given -- is already set up in the setUp and before methods

    //when
    ByteArrayInputStream in = new ByteArrayInputStream("3.5".getBytes());
    System.setIn(in);
    DoroshenkoQuestion001.main(EMPTY_ARGS);

    //then
    Assert.assertEquals("жёлтый" + LINE_DELIMITER, outStream.toString());
  }

  @Test
  public void mainExpectsRed() {
    //given -- is already set up in the setUp and before methods

    //when
    ByteArrayInputStream in = new ByteArrayInputStream("4.5".getBytes());
    System.setIn(in);
    DoroshenkoQuestion001.main(EMPTY_ARGS);

    //then
    Assert.assertEquals("красный" + LINE_DELIMITER, outStream.toString());
  }

}